

def menu_opciones():
    opción = -1

    while opción != 0:

        print("\n~ ANÁLISIS ESTADÍSTICO COVID-19 ~\n")
        print("1.Cantidad de casos confirmados (test positivo) y porcentaje sobre el total de casos.")
        print("2.Edad promedio de los pacientes que pertenecen a grupo de riesgo.")
        print("3.Cantidad y porcentaje que el personal de salud representa sobre el total de casos.")
        print("4.Edad promedio entre los casos confirmados.")
        print("5.Menor edad entre los casos autóctonos.")
        print("6.Cantidad de casos confirmados por región y porcentaje que representa cada uno sobre el total de casos.")
        print("7.Cantidad de casos confirmados con viaje al exterior.")
        print("8.Cantidad de casos sospechosos en contacto con casos confirmados.")
        print("9.Regiones sin casos confirmados.")
        print("10.Porcentaje de casos positivos autóctonos sobre el total de positivos.\n")
        opción = int(input("Ingrese una opción. (0 para salir): "))

        if opción == 1:
            pass
        elif opción == 2:
            pass
        elif opción == 3:
            pass
        elif opción == 4:
            pass
        elif opción == 5:
            pass
        elif opción == 6:
            pass
        print("~" * 80)

